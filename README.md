# pbconv_ui

## Criando a aplicação

```bash
# Clonar com o comando
$ https://gitlab.com/alex.barbosa/pbconv_ui.git

# Subir o container com
$ docker-compose up --build
```
## Backend da aplicação -> pbconv_api

Adicionar a url aqui

## ER do banco Atual

Adicionar a imagem do ER aqui.

## Arquitetura da aplicação

![image.png](./image.png)

## PBconv Git Flow

![image-1.png](./image-1.png)

## Documentações:
  * `NUXT` [referência](https://nuxtjs.org);
  * `ASSETS` [referência](https://nuxtjs.org/docs/2.x/directory-structure/assets).
  * `COMPONENTS` [referência](https://nuxtjs.org/docs/2.x/directory-structure/components).
  * `LAYOUTS` [referência](https://nuxtjs.org/docs/2.x/directory-structure/layouts).
  * `pages` [referência](https://nuxtjs.org/docs/2.x/get-started/routing).
  * `plugins` [referência](https://nuxtjs.org/docs/2.x/directory-structure/plugins).
  * `static` [referência](https://nuxtjs.org/docs/2.x/directory-structure/static).
  * `store` [referência](https://nuxtjs.org/docs/2.x/directory-structure/store).  
  * `VuePrime` [referência](https://www.primefaces.org/primevue-v2/#/
    * `Exemplo VuePrime` [referência](https://www.primefaces.org/sakai-vue/#/).
  * `PrimeFlex` [referência](https://www.primefaces.org/primeflex).
    * sistema de controle de layout: grid, display, etc.
