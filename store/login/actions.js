export default {
  authenticate ({ commit }, params) {
    return this.$auth.loginWith('local', {
      data: {
        ...params
      }
    })
  },
  async authLogout ({ commit }) {
    return await this.$auth.logout()
  }
}
