FROM node:lts-alpine

RUN apk add --no-cache python3 g++ make
RUN mkdir -p /var/www/pbconv/app
WORKDIR /var/www/pbconv/app

COPY package*.json ./
RUN yarn install

COPY . .
RUN apk --no-cache --virtual build-dependencies add \
        python3 \
        make \
        g++

RUN yarn build

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=3000

CMD [ "yarn", "start" ]
